from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('icecreamtruck-api/', include('icecreamtruck.urls', namespace='icecreamtruck-api')),
    path('admin/', admin.site.urls),
]
