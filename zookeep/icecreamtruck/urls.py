from rest_framework import routers
from django.urls import path, include

from icecreamtruck import api

app_name = "icecreamtruck-api"

router = routers.DefaultRouter()
router.register(r'truck-inventory-sales', api.TruckInventorySalesViewSet, basename="truck-inventory-sales")
router.register(r'customer-feedback', api.CustomerFeedbackViewSet, basename="customer-feedback")
router.register(r'truck-order-history', api.TruckOrderHistoryViewSet, basename="truck-order-history")
router.register(r'truck-item-status', api.TruckItemStatusViewSet, basename="truck-item-status")

urlpatterns = [
    path('', include(router.urls)),
    path('customer-order/', api.CustomerOrderAPIView.as_view(), name="customer-order")
]