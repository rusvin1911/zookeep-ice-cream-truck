import random
import decimal
import json

from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse

from icecreamtruck.models import IceCreamTruck, FoodItems
from user.models import CustomerProfile

class IceCreamTruckOrderTest(TestCase):
    client = Client()

    def setUp(self):
        # Creating necessary object instances
        DEFAULT_ICE_CREAM_TRUCK_NAME = "Ice Cream Truck 1"
        DEFAULT_CUSTOMER_USERNAME = "customer_1"
        DEFAULT_CUSTOMER_NAME = "Customer 1"
        
        User = get_user_model()

        # Create IceCreamTruck instance
        truck = IceCreamTruck.objects.create(
            name = DEFAULT_ICE_CREAM_TRUCK_NAME,
        )

        # Create FoodItems instances
        food_list = FoodItems.FOOD_ITEM_CHOICES

        for food in food_list:
            current_food = food[0]

            # Generating random values for the price (between $1-$20) and quantity(between 1-10)
            price = round(decimal.Decimal(random.uniform(1,20)), 2)
            quantity = random.randint(1, 10)

            # Handle icecream instances
            if current_food == "ice_cream":
                flavor_list = FoodItems.FOOD_FLAVOR_CHOICES

                for flavor in flavor_list:
                    FoodItems.objects.create(
                        truck = truck,
                        item = current_food,
                        flavor = flavor[0],
                        price = price,
                        current_quantity = quantity,
                        initial_quantity = quantity,

                    )
            else:
                FoodItems.objects.create(
                    truck = truck,
                    item = current_food,
                    price = price,
                    current_quantity = quantity,
                    initial_quantity = quantity,
                )
        
        # Create User & CustomerProfile instances
        user = User.objects.create(username=DEFAULT_CUSTOMER_USERNAME)

        CustomerProfile.objects.create(user=user, name=DEFAULT_CUSTOMER_NAME)

        # Setting endpoints for testing
        self.customer_order_endpoint = reverse("icecreamtruck-api:customer-order")

    def test_successfull_customer_order(self):
        """
        1. Testing for successful user order when all the food items are in stock
        Endpoint: /icecreamtruck-api/customer-order/
        Expected Result: Customer order is accepted and the response will be "ENJOY!"
        """
        ENDPOINT_SUCCESS_MESSAGE = "ENJOY!"

        truck = IceCreamTruck.objects.get(id=1)
        customer = CustomerProfile.objects.get(id=1)

        # Obtaining random size of food items
        food_items_id = FoodItems.objects.filter(truck=truck).values_list("id", flat=True)
        food_item_count = food_items_id.count()
        random_food_count = random.randint(1, food_item_count)
        random_food_id = random.sample(list(food_items_id), random_food_count)
        food_items = FoodItems.objects.filter(id__in=random_food_id)

        customer_order_body = []
        for food in food_items:
            # Setting random quantity to order that is within the current limit
            max_item_qty = food.current_quantity
            qty_to_order = random.randint(1, max_item_qty)

            customer_order_body.append({
                "food_id" : food.id,
                "item" : food.item,
                "flavor": food.flavor,
                "quantity": qty_to_order
            })
        
        body = {
            "customer_id": customer.id,
            "truck_id": truck.id,
            "customer_order" : customer_order_body
            }
        
        response = self.client.post(self.customer_order_endpoint, data=json.dumps(body), content_type='application/json')

        self.assertEqual(response.data, ENDPOINT_SUCCESS_MESSAGE)
        self.assertEqual(response.status_code, 200)

    def test_unsuccessfull_customer_order(self):
        """
        2. Testing for unsuccessful user order when the user attempts to order more than what the truck currently has
        Endpoint: /icecreamtruck-api/customer-order/
        Expected Result: Customer order is not accepted and the response will be "SORRY!"
        """
        ENDPOINT_ERROR_MESSAGE = "SORRY!"

        truck = IceCreamTruck.objects.get(id=1)
        customer = CustomerProfile.objects.get(id=1)

        # Obtaining random size of food items
        food_items_id = FoodItems.objects.filter(truck=truck).values_list("id", flat=True)
        food_item_count = food_items_id.count()
        random_food_count = random.randint(1, food_item_count)
        random_food_id = random.sample(list(food_items_id), random_food_count)
        food_items = FoodItems.objects.filter(id__in=random_food_id)

        customer_order_body = []
        for food in food_items:
            # Setting random quantity that is outside of the current limit
            max_item_qty = food.current_quantity
            qty_to_order = random.randint(max_item_qty, 999)

            customer_order_body.append({
                "food_id" : food.id,
                "item" : food.item,
                "flavor": food.flavor,
                "quantity": qty_to_order
            })
        
        body = {
            "customer_id": customer.id,
            "truck_id": truck.id,
            "customer_order" : customer_order_body
            }
        
        response = self.client.post(self.customer_order_endpoint, data=json.dumps(body), content_type='application/json')

        self.assertEqual(response.data, ENDPOINT_ERROR_MESSAGE)
        self.assertEqual(response.status_code, 200)
    

    def test_empty_customer_order(self):
        """
        3. Testing for when an order is submitted without any items in it
        Endpoint: /icecreamtruck-api/customer-order/
        Expected Result: HTTP 400 (Bad Request) error along with the corresponding error detail
        """
        truck = IceCreamTruck.objects.get(id=1)
        customer = CustomerProfile.objects.get(id=1)
        
        body = {
            "customer_id": customer.id,
            "truck_id": truck.id,
            "customer_order" : None
            }
        
        response = self.client.post(self.customer_order_endpoint, data=json.dumps(body), content_type='application/json')

        self.assertEqual(response.status_code, 400)
