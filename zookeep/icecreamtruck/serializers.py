import decimal

from rest_framework import serializers

from .models import IceCreamTruck, FoodItems, CustomerFeedback, TruckOrderHistory


class FoodItemsSerializer(serializers.ModelSerializer):
    food_id = serializers.CharField(source="id")

    class Meta:
        model = FoodItems
        fields = ("food_id", "item", "flavor", "price", "initial_quantity", "current_quantity")


class TruckInventorySalesSerializer(serializers.ModelSerializer):
    truck_id = serializers.CharField(source="id")
    truck_items = FoodItemsSerializer(many=True, read_only=True)
    total_profit = serializers.SerializerMethodField()

    class Meta:
        model = IceCreamTruck
        fields = ("truck_id", "name", "total_profit", "rating", "truck_items")
    
    def get_total_profit(self, obj):
        # Obtain profit made for a Ice Cream Truck
        truck_item_list = FoodItems.objects.filter(truck=obj)
        total_profit = decimal.Decimal("0.00")
        
        for item in truck_item_list:
            quantity_sold = item.initial_quantity - item.current_quantity
            profit = item.price * decimal.Decimal(quantity_sold)
            total_profit += profit
        
        return total_profit


class CustomerItemOrderSerializer(serializers.ModelSerializer):
    food_id = serializers.CharField(source="id")
    quantity = serializers.IntegerField()

    class Meta:
        model = FoodItems
        fields = ("food_id", "item", "flavor", "quantity")


class CustomerOrderSerializer(serializers.Serializer):
    customer_id = serializers.CharField(max_length=24)
    truck_id = serializers.CharField(max_length=24)
    customer_order = CustomerItemOrderSerializer(many=True)


class CustomerFeedbackSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomerFeedback
        fields = "__all__"


class TruckOrderHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = TruckOrderHistory
        fields = "__all__"

    
