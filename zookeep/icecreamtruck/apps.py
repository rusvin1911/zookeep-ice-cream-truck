from django.apps import AppConfig


class IcecreamtruckConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'icecreamtruck'

    def ready(self):
        from . import signals
