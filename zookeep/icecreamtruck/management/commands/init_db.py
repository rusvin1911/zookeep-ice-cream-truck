import random
import decimal

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from icecreamtruck.models import IceCreamTruck, FoodItems
from user.models import CustomerProfile

User = get_user_model()

class Command(BaseCommand):
    help = "Creates the model instances that are required in order to use the IceCreamTruck API"

    def handle(self, *args, **options):
        # safety check to ensure that no more than 1 IceCreamTruck object is created
        ice_cream_truck_created = IceCreamTruck.objects.exists()
        food_items_created = FoodItems.objects.exists()
        customer_profile_created = CustomerProfile.objects.exists()

        if ice_cream_truck_created or food_items_created or customer_profile_created:
            print("There are existing model instances in the database. Aborting...")
            return
        
        DEFAULT_ICE_CREAM_TRUCK_NAME = "Ice Cream Truck 1"

        # Create IceCreamTruck instance
        truck = IceCreamTruck.objects.create(
            name = DEFAULT_ICE_CREAM_TRUCK_NAME,
        )
        print("Ice Cream Truck created...")

        # Create FoodItems instances
        food_list = FoodItems.FOOD_ITEM_CHOICES

        for food in food_list:
            current_food = food[0]

            # Generating random values for the price and quantity
            price = round(decimal.Decimal(random.uniform(1,20)), 2)
            quantity = random.randint(1, 10)

            # Handle icecream instances
            if current_food == "ice_cream":
                flavor_list = FoodItems.FOOD_FLAVOR_CHOICES

                for flavor in flavor_list:
                    FoodItems.objects.create(
                        truck = truck,
                        item = current_food,
                        flavor = flavor[0],
                        price = price,
                        current_quantity = quantity,
                        initial_quantity = quantity,

                    )
                    print("Ice Cream of flavor {} created...".format(flavor[1]))
            else:
                FoodItems.objects.create(
                    truck = truck,
                    item = current_food,
                    price = price,
                    current_quantity = quantity,
                    initial_quantity = quantity,
                )
                print("Food Item {} created...".format(food[1]))
        
        # Create User & CustomerProfile instances
        DEFAULT_CUSTOMER_USERNAME = "customer_1"
        DEFAULT_CUSTOMER_NAME = "Customer 1"

        user = User.objects.create(username=DEFAULT_CUSTOMER_USERNAME)

        CustomerProfile.objects.create(user=user, name=DEFAULT_CUSTOMER_NAME)
        print("Customer Profile created...")

        print("Database initialization completed...")