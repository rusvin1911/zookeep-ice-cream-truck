import math

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Count, Sum, F

from .models import CustomerFeedback


@receiver(post_save, sender=CustomerFeedback)
def update_truck_ratings(sender, instance, **kwargs):
    # only tigger whenever a new feedback is submitted
    if kwargs.get('created', True):
        truck = instance.truck

        rating_dict = sender.objects.filter(truck=truck)\
                                .values("rating")\
                                .annotate(frequency=Count("rating"))\
                                .aggregate(rating_total=Sum(F("rating") * F("frequency")), frequency_total=Sum("frequency"))

        truck_rating = rating_dict["rating_total"] / rating_dict["frequency_total"]
        truck_rating = round(truck_rating)

        # Update truck rating
        truck.rating = truck_rating
        truck.save()
