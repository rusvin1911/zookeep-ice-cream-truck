import decimal
import json

from rest_framework import viewsets, views, status
from rest_framework.serializers import ValidationError
from rest_framework.response import Response
from rest_framework.mixins import ListModelMixin
from rest_framework.decorators import action
from django.db.models import F

from user.models import CustomerProfile
from .models import FoodItems, IceCreamTruck, CustomerFeedback, TruckOrderHistory
from .serializers import TruckInventorySalesSerializer, CustomerOrderSerializer, CustomerFeedbackSerializer, TruckOrderHistorySerializer, FoodItemsSerializer


class TruckInventorySalesViewSet(viewsets.ModelViewSet):
    queryset = IceCreamTruck.objects.all()
    serializer_class = TruckInventorySalesSerializer


class CustomerOrderAPIView(views.APIView):
    
    def log_purchase_history(self, truck, food_items, sale_amount, customer):
        order_items = []
        for food in food_items:
            food_name = food.get_item_display()
            food_flavor = food.get_flavor_display()
            
            item_name = "{}".format(food_name)

            if food.flavor:
                item_name = "{} ({})".format(food_name, food_flavor)
    
            order_items.append(item_name)
        
        order_items = json.dumps(order_items)
        
        TruckOrderHistory.objects.create(truck=truck, items_purchased=order_items, sale_amount=sale_amount, customer=customer)
        
    def post(self, request):
        ORDER_SUCCESS_MESSAGE = "ENJOY!"
        ORDER_ERROR_MESSAGE = "SORRY!"

        serializer = CustomerOrderSerializer(data=request.data)
        if serializer.is_valid():
            customer_id = serializer.validated_data['customer_id']
            truck_id = serializer.validated_data['truck_id']
            customer_order = serializer.validated_data['customer_order']

            # Check if its a valid customer ID
            try:
                customer = CustomerProfile.objects.get(id=customer_id)
            except CustomerProfile.DoesNotExist:
                raise ValidationError({"detail": "This customer is not registered in the database"})

            # Check if its a valid truck ID
            try:
                truck = IceCreamTruck.objects.get(id=truck_id)
            except IceCreamTruck.DoesNotExist:
                raise ValidationError({"detail": "This truck is not registered in the database"})

            food_items = []
            sale_amount = decimal.Decimal("0.00")
            for item in customer_order:
                food_id = item["id"]
                food_name = item["item"]
                food_flavor = item["flavor"]
                quantity_purchased = item["quantity"]

                # Check if the food item exists and belongs to the right truck
                try:
                    food_item = FoodItems.objects.get(id=food_id, item=food_name, flavor=food_flavor, truck=truck)
                except FoodItems.DoesNotExist:
                    raise ValidationError({"detail": "This food item is not registered in the database"})

                # Checking if the order quantity for the item is valid
                updated_item_quantity = food_item.current_quantity - quantity_purchased
                if updated_item_quantity < 0 :
                    return Response(ORDER_ERROR_MESSAGE)
                
                food_item.current_quantity = updated_item_quantity
                food_items.append(food_item)
                sale_amount += food_item.price
            
            FoodItems.objects.bulk_update(food_items, ["current_quantity"])
            # Create log/receipt of order here
            self.log_purchase_history(truck, food_items, sale_amount, customer)

        else:
            raise ValidationError(serializer.errors)

        return Response(ORDER_SUCCESS_MESSAGE, status=status.HTTP_200_OK)


# Customer feedback endpoint
class CustomerFeedbackViewSet(viewsets.GenericViewSet, ListModelMixin):
    model = CustomerFeedback
    serializer_class = CustomerFeedbackSerializer
    queryset = CustomerFeedback.objects.all()

    def get_queryset(self):
        queryset = self.queryset

        truck_id = self.request.query_params.get('truck_id', None)
        if truck_id:
            queryset = queryset.filter(truck__id=truck_id)

        return queryset

    def create(self, request):
        serializer = CustomerFeedbackSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
        else:
            raise ValidationError(serializer.errors)
        
        return Response(None, status=status.HTTP_201_CREATED)


class TruckOrderHistoryViewSet(viewsets.ModelViewSet, ListModelMixin):
    queryset = TruckOrderHistory.objects.all()
    serializer_class = TruckOrderHistorySerializer
    
    def get_queryset(self):
        queryset = self.queryset

        # Allow filtering by individual truck/customers or both at the same time
        truck_id = self.request.query_params.get('truck_id', None)
        customer_id = self.request.query_params.get('customer_id', None)

        if truck_id:
            queryset = queryset.filter(truck__id=truck_id)
        
        if customer_id:
            queryset = queryset.filter(customer__id=customer_id)

        return queryset


class TruckItemStatusViewSet(viewsets.ModelViewSet, ListModelMixin):
    queryset = FoodItems.objects.all()
    serializer_class = FoodItemsSerializer

    def get_queryset(self):
        queryset = self.queryset

        truck_id = self.request.query_params.get('truck_id', None)
        if truck_id:
            queryset = queryset.filter(truck__id=truck_id)

        return queryset

    def get_serialized_data(self, queryset):
        serializer = self.serializer_class
        response = serializer(queryset, many=True).data

        return response

    @action(detail=False, methods=["get"], url_path="best-sellers")
    def best_sellers(self, request):
        queryset = self.get_queryset()
        #filter by most amount of items sold
        queryset = queryset.annotate(most_sold=F('initial_quantity') - F('current_quantity')).order_by('-most_sold')

        response = self.get_serialized_data(queryset)

        return Response(response, status=status.HTTP_200_OK)
    
    @action(detail=False, methods=["get"], url_path="in-stock")
    def in_stock(self, request):
        queryset = self.get_queryset()
        #filter items that are still in stock
        queryset = queryset.exclude(current_quantity=0).order_by('-current_quantity')

        response = self.get_serialized_data(queryset)

        return Response(response, status=status.HTTP_200_OK)
    
    @action(detail=False, methods=["get"], url_path="out-of-stock")
    def out_of_stock(self, request):
        queryset = self.get_queryset()
        #filter items that are still in stock
        queryset = queryset.exclude(current_quantity__gt=0)
        response = self.get_serialized_data(queryset)

        return Response(response, status=status.HTTP_200_OK)




