from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from user.models import CustomerProfile


class IceCreamTruck(models.Model):
    name = models.CharField(max_length=48, null=True)
    rating = models.IntegerField(default=0, validators=[MaxValueValidator(5), MinValueValidator(0)])

    def __str__(self):
        return str(self.name)


class FoodItems(models.Model):
    ICE_CREAM = "ice_cream"
    SHAVED_ICE = "shaved_ice"
    SNACK_BARS = "snack_bars"
    FOOD_ITEM_CHOICES = [
        (ICE_CREAM, "Ice Cream"),
        (SHAVED_ICE, "Shaved Ice"),
        (SNACK_BARS, "Snack Bars"),
    ]

    CHOCOLATE = "chocolate"
    PISTACHIO = "pistachio"
    STRAWBERRY = "strawberry" 
    MINT = "mint"    
    FOOD_FLAVOR_CHOICES = [
        (CHOCOLATE, "Chocolate"),
        (PISTACHIO, "Pistachio"),
        (STRAWBERRY, "Strawberry"),
        (MINT, "Mint")
    ]

    truck = models.ForeignKey("IceCreamTruck", related_name="truck_items", on_delete=models.PROTECT, null=True) 
    item = models.CharField(max_length=48, choices=FOOD_ITEM_CHOICES)
    flavor = models.CharField(max_length=48, choices=FOOD_FLAVOR_CHOICES, blank=True, null=True)
    price = models.DecimalField(decimal_places=2, max_digits=18)
    current_quantity = models.PositiveIntegerField()
    initial_quantity = models.PositiveIntegerField()


class CustomerFeedback(models.Model):
    truck = models.ForeignKey("IceCreamTruck", related_name="truck_reviews", on_delete=models.PROTECT, null=True)
    customer = models.ForeignKey(CustomerProfile, related_name="customer_feedback", on_delete=models.PROTECT, null=True, blank=True)     #blank/null = True for anonymous reviews
    comment = models.CharField(max_length=1024, blank=True)
    rating = models.IntegerField(validators=[MaxValueValidator(5), MinValueValidator(0),], blank=True)


class TruckOrderHistory(models.Model):
    # model to keep track of all the transactions that were made in any specific truck
    customer = models.ForeignKey(CustomerProfile, related_name="customer_order_history", on_delete=models.PROTECT, null=True)
    truck = models.ForeignKey("IceCreamTruck", related_name="truck_order_history", on_delete=models.PROTECT, null=True)
    items_purchased = models.JSONField()
    sale_amount = models.DecimalField(decimal_places=2, max_digits=18)

