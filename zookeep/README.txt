Quick Start Guide to run the project & access the endpoints.

1. Project is built with python version 3.10

2. Upon installing the required dependencies and setting up the virtual environment, run 'python manage.py migrate' to apply the necessary migrations

3. Run 'python manage.py init_db' in order to load up dummy instances into the models for testing


API Documentation
Following are a list of all the endpoints that are available in this project and a brief description & example use case:
NOTE: This is not a full documentation. Only meant to quickly illustrate the functionalities of each endpoint

1. Truck Inventory & Sales 
   Description : Obtains a list of all trucks in the system along with their inventory & sales details. Endpoint is able to filter by specific trucks by passing in the individual truck ID
   Endpoint: /icecreamtruck-api/truck-inventory-sales/
   Parameters : None
   Method: GET
   Request Body: None

   Response:[
    {
        "truck_id": "1",
        "name": "Ice Cream Truck 1",
        "total_profit": 54.86,
        "rating": 4,
        "truck_items": [
            {
                "food_id": "1",
                "item": "ice_cream",
                "flavor": "chocolate",
                "price": "3.46",
                "initial_quantity": 3,
                "current_quantity": 3
            },
            {
                "food_id": "2",
                "item": "ice_cream",
                "flavor": "pistachio",
                "price": "3.46",
                "initial_quantity": 3,
                "current_quantity": 3
            },
            {
                "food_id": "3",
                "item": "ice_cream",
                "flavor": "strawberry",
                "price": "3.46",
                "initial_quantity": 3,
                "current_quantity": 3
            },
            {
                "food_id": "4",
                "item": "ice_cream",
                "flavor": "mint",
                "price": "3.46",
                "initial_quantity": 3,
                "current_quantity": 0
            },
            {
                "food_id": "5",
                "item": "shaved_ice",
                "flavor": null,
                "price": "18.28",
                "initial_quantity": 10,
                "current_quantity": 8
            },
            {
                "food_id": "6",
                "item": "snack_bars",
                "flavor": null,
                "price": "1.98",
                "initial_quantity": 6,
                "current_quantity": 2
            }
        ]
    }
]

2. Customer Order 
   Description : Allows customers to place orders to a specific food truck
   Endpoint: /icecreamtruck-api/customer-order/
   Parameters : None
   Method: POST
   Request Body: {
    "customer_id": 1,
    "truck_id": 1,
    "customer_order" : [
        {
            "food_id" : 5,
            "item" : "shaved_ice",
            "flavor": null,
            "quantity": 1
        },
        {
            "food_id" : 6,
            "item" : "snack_bars",
            "flavor": null,
            "quantity": 1
        }
    ]
}

    Response : "ENJOY!"

3. Customer Feedback (GET)
   Description : Obtains a list of all feedback left by customers. Able to filter by specific trucks as well
   Endpoint: /icecreamtruck-api/customer-feedback/
   Parameters : truck_id
   Method: GET
   Request Body: None

   Response : [
    {
        "id": 1,
        "comment": "Testing Comment",
        "rating": 3,
        "truck": 1,
        "customer": null
    },
    {
        "id": 2,
        "comment": "Testing Comment",
        "rating": 3,
        "truck": 1,
        "customer": null
    },
    {
        "id": 3,
        "comment": "Testing Comment",
        "rating": 3,
        "truck": 1,
        "customer": null
    },
]

4. Customer Feedback (POST)
   Description : Allows customers to leave a feedback & rating for a particular truck
   Endpoint: /icecreamtruck-api/customer-feedback/
   Parameters : None
   Method: POST
   Request Body: {
    "truck": 1,
    "customer": 1,
    "comment": "Testing Comment",
    "rating": 5
}

   Response : None (HTTP 200)

5. Truck Order History
   Description : Obtains a list of all the successul order done. Able to filter by any specific truck/customer or both
   Endpoint: /icecreamtruck-api/truck-order-history/
   Parameters : truck_id, customer_id
   Method: GET
   Request Body: None

   Response : [
    {
        "id": 3,
        "items_purchased": "[\"Shaved Ice\", \"Snack Bars\"]",
        "sale_amount": "20.26",
        "customer": 1,
        "truck": 1
    },
    {
        "id": 4,
        "items_purchased": "[\"Shaved Ice\", \"Snack Bars\"]",
        "sale_amount": "20.26",
        "customer": 1,
        "truck": 1
    }
]

6. Truck Item Status
   Description : Obtains a list of all the items that are still in stock. Able to filter by specific truck as well
   Endpoint: /icecreamtruck-api/truck-item-status/in-stock/
   Parameters : truck_id
   Method: GET
   Request Body: None

   Response : [
    {
        "food_id": "5",
        "item": "shaved_ice",
        "flavor": null,
        "price": "18.28",
        "initial_quantity": 10,
        "current_quantity": 8
    },
    {
        "food_id": "1",
        "item": "ice_cream",
        "flavor": "chocolate",
        "price": "3.46",
        "initial_quantity": 3,
        "current_quantity": 3
    },
    {
        "food_id": "2",
        "item": "ice_cream",
        "flavor": "pistachio",
        "price": "3.46",
        "initial_quantity": 3,
        "current_quantity": 3
    },
    {
        "food_id": "3",
        "item": "ice_cream",
        "flavor": "strawberry",
        "price": "3.46",
        "initial_quantity": 3,
        "current_quantity": 3
    },
    {
        "food_id": "6",
        "item": "snack_bars",
        "flavor": null,
        "price": "1.98",
        "initial_quantity": 6,
        "current_quantity": 2
    }
]

7. Truck Item Status
   Description : Obtains a list of all the items that are the best sellers. Able to filter by specific truck as well
   Endpoint: /icecreamtruck-api/truck-item-status/best-sellers/
   Parameters : truck_id
   Method: GET
   Request Body: None

   Response : [
    {
        "food_id": "6",
        "item": "snack_bars",
        "flavor": null,
        "price": "1.98",
        "initial_quantity": 6,
        "current_quantity": 2
    },
    {
        "food_id": "4",
        "item": "ice_cream",
        "flavor": "mint",
        "price": "3.46",
        "initial_quantity": 3,
        "current_quantity": 0
    },
    {
        "food_id": "5",
        "item": "shaved_ice",
        "flavor": null,
        "price": "18.28",
        "initial_quantity": 10,
        "current_quantity": 8
    },
    {
        "food_id": "1",
        "item": "ice_cream",
        "flavor": "chocolate",
        "price": "3.46",
        "initial_quantity": 3,
        "current_quantity": 3
    },
    {
        "food_id": "2",
        "item": "ice_cream",
        "flavor": "pistachio",
        "price": "3.46",
        "initial_quantity": 3,
        "current_quantity": 3
    },
    {
        "food_id": "3",
        "item": "ice_cream",
        "flavor": "strawberry",
        "price": "3.46",
        "initial_quantity": 3,
        "current_quantity": 3
    }
]

8. Truck Item Status
   Description : Obtains a list of all the items that are out of stock. Able to filter by specific truck as well
   Endpoint: /icecreamtruck-api/truck-item-status/out-of-stock/
   Parameters : truck_id
   Method: GET
   Request Body: None

   Response : [
    {
        "food_id": "4",
        "item": "ice_cream",
        "flavor": "mint",
        "price": "3.46",
        "initial_quantity": 3,
        "current_quantity": 0
    }
]